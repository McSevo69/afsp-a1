#include "keygen.h"

void initTigress() {}

int getFibNo(int fib) {
	int first = 0, second = 1, res = 1;

	for (int j = 0; j < fib + 1; ++j) {
		res = first + second;
		first = second;
		second = res;
	}

	return res;
}

int combineCharMulti(char charUser, char charKey) {
	int res = (int) charUser * (int) charKey;

	#ifdef DEBUG
	printf("Debug: Result combineCharMulti = %i\n", (int) res);
	#endif

	return res;
}

void generateLicense(char* user, char* license) {

	int progUser, progKey;

	for (int i = 0; i < keyLength; ++i) {
		#ifdef DEBUG
		printf("Debug: Calculating Fibonacci numbers for user and key\n");
		#endif
		progUser = getFibNo(i);
		progKey = getFibNo(i+1);

		#ifdef DEBUG
		printf("Debug: Get character from user and key\n");
		#endif
		char charUser = user[progUser % strlen(user)];
		char charKey = key[progKey % strlen(key)];

		#ifdef DEBUG
		printf("Debug: Calculate character based on user and key characters\n");
		#endif
		char licenseChar = alphabet[combineCharMulti(charUser, charKey) % strlen(alphabet)];

		#ifdef DEBUG
		printf("Debug: Append first character of %s to license string %s\n", &licenseChar, license);
		#endif 
		strncat(license, &licenseChar, 1);
	}
}

void showHelpMessage(char *argv[]) {
	printf("================================USAGE================================\n");
	printf("%s [options]\n", argv[0]);
	printf("  -h | --help\t\tShow this help message\n");
	printf("  -u | --user\t\tUsername (at least length 3)\n");
	printf("alternative usage:\n");
	printf("%s <username>\n", argv[0]);
	printf("=====================================================================\n");
}

int convertArgToInt(char * str) {
	if (strcmp ("--help", str) == 0 || strcmp ("-h", str) == 0) return 1;
	else if (strcmp ("--user", str) == 0 || strcmp ("-u", str) == 0) return 2;
	else return 0;
}

int main(int argc, char *argv[]) {

	initTigress();
	
	printf("=====================================================================\n");
	printf("                 #  # #### #    # ####  #### ##    #                 \n");
	printf("                 # #  #     #  #  #     #    # #   #                 \n");
	printf("                 ##   ###    #    # ##  ###  #  #  #                 \n");
	printf("                 # #  #      #    #  #  #    #   # #                 \n");
	printf("                 #  # ####   #    ####  #### #    ##                 \n");
	printf("=====================================================================\n");
	
	char *user = "";

	#ifdef DEBUG
	printf("Debug: Parsing command line parameters\n");
	#endif

	for (int i=1; i<argc; i++) {
		switch(convertArgToInt(argv[i])) {
	    		case 1: showHelpMessage(argv); return 0;
			case 2: user = argv[++i]; break;
			default: break;
		}
	}

	if (argc == 2) {
		user = argv[1];
	}

	#ifdef DEBUG
	printf("Debug: Checking username length\n");
	#endif

	if (strlen(user) < 3) {
		printf("Error: Username \"%s\" is too short. Minimum length: %i\n", user, minLengthUser);
		return -1;
	}

	#ifdef DEBUG
	printf("Debug: Generating License for user %s\n", user);
	#endif
	char *license = malloc(sizeof(char) * (keyLength + 1));
	generateLicense(user, license);

	printf("The license for the user \"%s\" is \"%s\"\n", user, license);

	free(license);

	return 0;
}
