#include<stdio.h>
#include<string.h>
#include<stdlib.h>

char* generate(char* user);
int get_progression_fib(int i);
char combine_char_multi(char user, char key);

int main(int argc, char *argv[]) {
    char* user, *license;
    /* printf("arg size: %d arg length: %lu", argc, strlen(argv[1])); */
    /* Check if there is one and only one argument. */
    if (argc != 2) {
        printf("Program only takes 1 argument.\n");
        return 1;
    }

    user = argv[1];

    if (strlen(user) < 3) {
        printf("User has to be at least 3 chars long.\n");
        return 1;

    }

    license = generate(user);
    printf("The license key for the user '%s' is '%s'", user, license);
}

int get_progression_fib(int i) {
    int first = 0;
    int second = 1;
    int res = 1;
    for (int j = 1; j < i+1; j++) {
        res = first + second;
        first = second;
        second = res;
    }
    return res;
}

char combine_char_multi(char user, char key) {
    int res = user * key;
    char converted_res = (char) res;
    return converted_res;
}

char* generate(char* user) {
    char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    char key[] = "TheSecretStaticKey";
    int length = 10;
    char* license = malloc(sizeof(char) * length);
    printf("generate started...\n");
    for (int i = 0; i<length; i++) {
        /* Calculate which character to take next (the key is always one ahead */
        int progression_user = get_progression_fib(i);
        int progression_key = get_progression_fib(i+1);
        /* Get the character from the user and the key */
        char user_ch = user[progression_user % strlen(user)];
        char key_ch = key[progression_key % strlen(key)];
        /* Calculate the next character based on the user and key character */
        char license_ch = alphabet[ ((int) combine_char_multi(user_ch, key_ch)) % strlen(alphabet)];
        /* Append the character to the license */
        strncat(license, &license_ch, 1);

    }
    return license;
}    
