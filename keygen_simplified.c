#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char*	alphabet      = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
static const char*	key           = "TheSecretStaticKey";
static const int	keyLength     = 10;
static const int	minLengthUser = 3;

void initTigress() {}

int getFibNo(int fib) {
	int first = 0, second = 1, res = 1;

	for (int j = 0; j < fib + 1; ++j) {
		res = first + second;
		first = second;
		second = res;
	}
	return res;
}

int combineCharMulti(char charUser, char charKey) {
	return (int) charUser * (int) charKey;
}

void generateLicense(char* user, char* license) {

	int progUser, progKey;

	for (int i = 0; i < keyLength; ++i) {
		progUser = getFibNo(i);
		progKey = getFibNo(i+1);

		char charUser = user[progUser % strlen(user)];
		char charKey = key[progKey % strlen(key)];

		char licenseChar = alphabet[combineCharMulti(charUser, charKey) % strlen(alphabet)];

		strncat(license, &licenseChar, 1);
	}
}

int main(int argc, char *argv[]) {

	initTigress();

	char *user = "";

	if (argc == 2) user = argv[1];

	if (strlen(user) < 3) {
		printf("Error: Username \"%s\" is too short. Minimum length: %i\n", user, minLengthUser);
		return -1;
	}

	char *license = (char *) malloc(sizeof(char) * (keyLength + 1));
	generateLicense(user, license);

	printf("The license for the user \"%s\" is \"%s\"\n", user, license);

	free(license);

	return 0;
}
