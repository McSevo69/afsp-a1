#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//constant values a.k.a. global variables

static const char*	alphabet      = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
static const char*	key           = "TheSecretStaticKey";
static const int	keyLength     = 10;
static const int	minLengthUser = 3;

//function declarations

int	getFibNo(int fib);
int	combineCharMulti(char charUser, char charKey);
void	generateLicense(char* stringUser, char* license);
