CC := gcc 
OBJECTS := keygen_simplified.c
LIBS := -lm
INCLUDE := -I.
OUT := generated/tigress
FUNC := generateLicense,combineCharMulti,getFibNo
ENV ?= x86_64:Linux:Gcc:4.6

#CFLAGS := -Wall -O3	#turned off to make analyzing compiled code easier
#DEBUG := -DDEBUG	#turn on for debug messages

.Phony:
all : main tigress compile_tigress

main : $(OBJECTS)
	$(CC) -o binaries/main $(DEBUG) $(OBJECTS) $(CFLAGS) $(INCLUDE) $(LIBS)

%.o : %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(INCLUDE)

tigress: main_measure encode opaque flatten virtualize comb1 comb2 comb3

compile_tigress: compile_main_measure compile_encode compile_opaque compile_flatten compile_virtualize compile_comb1 compile_comb2 compile_comb3

main_measure:
	tigress --Environment=$(ENV) --Transform=Measure --Functions=$(FUNC) --MeasureTimes=1 \
	--Functions=main,generateLicense --out=$(OUT)_mainmeasure.c $(OBJECTS)
	
encode:
	tigress --Environment=$(ENV) --Transform=EncodeData --GlobalVariables='keyLength,minLengthUser' \
	--LocalVariables='generateLicense:progUser,progKey;getFibNo:fib,first,second,res' --Functions=$(FUNC) \
	--Transform=Measure --MeasureTimes=1 --Functions=main,generateLicense \
	--out=$(OUT)_dataencode.c $(OBJECTS)
	
opaque:
	tigress --Environment=$(ENV) --Transform=InitOpaque --Functions=initTigress \
	--Transform=UpdateOpaque --Functions=$(FUNC) \
	--Transform=AddOpaque --Functions=$(FUNC) --AddOpaqueCount=2 \
	--Transform=Measure --Functions=main,generateLicense --MeasureTimes=1 \
	--out=$(OUT)_opaque.c $(OBJECTS)

flatten:
	tigress --Environment=$(ENV) --Transform=Flatten --Functions=$(FUNC) \
	--Transform=Measure --Functions=main,generateLicense --MeasureTimes=1 \
	--out=$(OUT)_flatten.c $(OBJECTS)	
	
virtualize:
	tigress --Environment=$(ENV) --Transform=Virtualize --Functions=$(FUNC) --VirtualizeDispatch=direct \
	--Transform=Measure --Functions=main,generateLicense --MeasureTimes=1 \
	--out=$(OUT)_virtualize.c $(OBJECTS)

comb1:
	tigress --Environment=$(ENV) --Transform=InitEntropy --Functions=initTigress --InitEntropyKinds=vars \
	--Transform=InitOpaque --Functions=initTigress --InitOpaqueStructs=list,array,env \
	--Transform=InitBranchFuns --InitBranchFunsCount=1 \
	--Transform=AddOpaque --Functions=$(FUNC) --AddOpaqueStructs=list --AddOpaqueKinds=true \
	--Transform=AntiBranchAnalysis --Functions=$(FUNC) --AntiBranchAnalysisKinds=branchFuns \
	--AntiBranchAnalysisObfuscateBranchFunCall=false --AntiBranchAnalysisBranchFunFlatten=true \
	--Transform=EncodeArithmetic --Functions=$(FUNC) \
	--Transform=Measure --Functions=main,generateLicense --MeasureTimes=1 \
	--out=$(OUT)_comb1.c $(OBJECTS)

comb2:
	tigress --Environment=$(ENV) --Transform=InitEntropy --Functions=initTigress --InitEntropyKinds=vars \
	--Transform=InitOpaque --Functions=initTigress --InitOpaqueStructs=list,array,env \
	--Transform=Merge --MergeFlatten=false --MergeName=MERGED --Functions=$(FUNC) \
	--Transform=Virtualize --VirtualizeDispatch=direct --Functions=MERGED \
	--Transform=EncodeLiterals --Functions=main,MERGED \
	--Transform=Measure --Functions=main,MERGED --MeasureTimes=1 \
	--out=$(OUT)_comb2.c $(OBJECTS)

comb3:
	tigress -ldl --Environment=$(ENV) --Transform=InitEncodeExternal \
	--Functions=initTigress --InitEncodeExternalSymbols=strlen,strncat \
	--Transform=EncodeLiterals --Functions=$(FUNC),initTigress \
	--EncodeLiteralsKinds=integer,string --EncodeLiteralsEncoderName=STRINGENCODER \
	--Transform=Virtualize --Functions=STRINGENCODER \
	--Transform=EncodeExternal --Functions=generateLicense \
	--EncodeExternalSymbols=strlen,strncat \
	--Transform=Flatten --Functions=$(FUNC) \
	--Transform=Measure --Functions=main,generateLicense --MeasureTimes=1 \
	--out=$(OUT)_comb3.c $(OBJECTS)
	
compile_main_measure:
	$(CC) -o binaries/main_measure_bin $(OUT)_mainmeasure.c $(CFLAGS) $(INCLUDE) $(LIBS)
	
compile_encode:
	$(CC) -o binaries/encode_bin $(OUT)_dataencode.c $(CFLAGS) $(INCLUDE) $(LIBS)
	
compile_opaque:
	$(CC) -o binaries/opaque_bin $(OUT)_opaque.c $(CFLAGS) $(INCLUDE) $(LIBS)
	
compile_flatten:
	$(CC) -o binaries/flatten_bin $(OUT)_flatten.c $(CFLAGS) $(INCLUDE) $(LIBS)
	
compile_virtualize:
	$(CC) -o binaries/virtualize_bin $(OUT)_virtualize.c $(CFLAGS) $(INCLUDE) $(LIBS)

compile_comb1:
	$(CC) -o binaries/comb1_bin $(OUT)_comb1.c $(CFLAGS) $(INCLUDE) $(LIBS)
	
compile_comb2:
	$(CC) -o binaries/comb2_bin --static -Wl,--omagic $(OUT)_comb2.c $(CFLAGS) $(INCLUDE) $(LIBS)
	
compile_comb3:
	$(CC) -o binaries/comb3_bin $(OUT)_comb3.c $(CFLAGS) $(INCLUDE) -ldl $(LIBS)

clean:
	for file in binaries/* *.o; do if [ -f $${file} ]; then rm $${file}; fi; done	

run: main
	./binaries/main "username"
